package com.jeeranan132.testmidterm;

public class Function {
    public static int resultSum1(int num1, int num2) { // ฟังก์ชั่นที่มีการส่งค่ากลับ
        int sum;
        sum = num1 + num2;
        return sum;
    }

    public static int resultSum2(int num1, int num2) { // ฟังก์ชั่นที่มีการส่งค่ากลับ
        return num1 + num2;
    }

    public static void printMessage() { // ฟังก์ชั่นที่ไม่มีการส่งค่ากลับ
        System.out.println("Hello form Java");
    }

    public static void main(String[] args) {
        System.out.println(resultSum1(10, 20)); // การเรียกใช้ฟังก์ชั่น
        System.out.println(resultSum2(10, 20));
        printMessage();
    }

}
